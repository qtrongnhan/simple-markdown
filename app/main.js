const electron = require('electron');
const { app, BrowserWindow } = electron;

function createWindow() {
  win = new BrowserWindow({
    show: false,
    hasShadow: true,
    titleBarStyle: 'hidden',
    resizable: true,
    fullscreenable: true,
    simpleFullscreen: true,
    skipTaskbar: false,
    fullscreenWindowTitle: true,
    transparent: true,
  });

  win.loadFile('simple-markdown/index.html');

  /* Show the window when the app is fully loaded
  * Remove this if you want to show the empty window for faster user's experience
  */
  win.once('ready-to-show', () => {
    win.show();
  });
}

app.on('ready', createWindow);

