(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/about-dialog/about-dialog.component.css":
/*!*********************************************************!*\
  !*** ./src/app/about-dialog/about-dialog.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".close-button {\r\n  display: flex;\r\n  flex: 1;\r\n  justify-content: center;\r\n}"

/***/ }),

/***/ "./src/app/about-dialog/about-dialog.component.html":
/*!**********************************************************!*\
  !*** ./src/app/about-dialog/about-dialog.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2 mat-dialog-title>About</h2>\r\n<mat-dialog-content>\r\n  <h3>Mark down is awesome</h3>\r\n  <p>\r\n    Created with Angular & Electron & ngx-markdown\r\n  </p>\r\n  <p>\r\n    Checkout more information about Markdown here:\r\n    <a href=\"https://www.markdownguide.org/\" target=\"_blank\">https://www.markdownguide.org/</a>\r\n  </p>\r\n</mat-dialog-content>\r\n<mat-dialog-actions>\r\n  <button class=\"close-button\" mat-stroked-button mat-dialog-close>Close</button>\r\n</mat-dialog-actions>"

/***/ }),

/***/ "./src/app/about-dialog/about-dialog.component.ts":
/*!********************************************************!*\
  !*** ./src/app/about-dialog/about-dialog.component.ts ***!
  \********************************************************/
/*! exports provided: AboutDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutDialogComponent", function() { return AboutDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var AboutDialogComponent = /** @class */ (function () {
    function AboutDialogComponent(data) {
        this.data = data;
    }
    AboutDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'about-dialog',
            template: __webpack_require__(/*! ./about-dialog.component.html */ "./src/app/about-dialog/about-dialog.component.html"),
            styles: [__webpack_require__(/*! ./about-dialog.component.css */ "./src/app/about-dialog/about-dialog.component.css")]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object])
    ], AboutDialogComponent);
    return AboutDialogComponent;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".middle-full-width {\r\n  display: flex;\r\n  flex: 1;\r\n}\r\n\r\n.currentView {\r\n  font-weight: bold;\r\n  color: indigo;\r\n}\r\n"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"primary\" style=\"position: fixed; z-index: 10;\">\r\n  <mat-toolbar-row>\r\n    <span>Simple Markdown</span>\r\n    <span class=\"middle-full-width\"></span>\r\n    <span>\r\n        <button mat-icon-button [matMenuTriggerFor]=\"viewModeMenu\">\r\n          <i class=\"material-icons\">visibility</i>\r\n        </button>\r\n        <mat-menu #viewModeMenu=\"matMenu\">\r\n          <button mat-menu-item (click)=\"activateDualMode()\">\r\n            <span [ngClass]=\"{'currentView': currentView === 'dual'}\">Dual</span>\r\n          </button>\r\n          <button mat-menu-item (click)=\"activatePreviewMode()\">\r\n            <span [ngClass]=\"{'currentView': currentView === 'preview'}\">Preview Only</span>\r\n          </button>\r\n          <button mat-menu-item (click)=\"activateOriginalMode()\">\r\n              <span [ngClass]=\"{'currentView': currentView === 'original'}\">Original Only</span>\r\n          </button>\r\n        </mat-menu>\r\n      </span>\r\n    <span>\r\n        <input type=\"file\" id=\"fileUpload\" #fileUpload style=\"display: none;\" (change)=\"readFile();\">\r\n        <button mat-icon-button (click)=\"fileUpload.click();\">\r\n          <i class=\"material-icons\">folder</i>\r\n        </button>\r\n      </span>\r\n    <span>\r\n      <button mat-icon-button [matMenuTriggerFor]=\"saveAsMenu\">\r\n        <i class=\"material-icons\">save</i>\r\n      </button>\r\n      <mat-menu #saveAsMenu=\"matMenu\">\r\n        <button mat-menu-item (click)=\"saveAsHtml()\">As HTML</button>\r\n        <button mat-menu-item (click)=\"saveAsMarkdown()\">As Markdown</button>\r\n      </mat-menu>\r\n    </span>\r\n    <span>\r\n      <button mat-icon-button [matMenuTriggerFor]=\"menu\">\r\n        <i class=\"material-icons\">more_vert</i>\r\n      </button>\r\n      <mat-menu #menu=\"matMenu\">\r\n        <button mat-menu-item (click)=\"openSettingsDialog()\">Settings</button>\r\n        <button mat-menu-item (click)=\"openAboutDialog()\">About</button>\r\n      </mat-menu>\r\n    </span>\r\n  </mat-toolbar-row>\r\n</mat-toolbar>\r\n<mat-sidenav-container class=\"example-container\" *ngIf=\"shouldRun\">\r\n  <mat-sidenav mode=\"side\" opened>Sidenav content</mat-sidenav>\r\n  <mat-sidenav-content>Main content</mat-sidenav-content>\r\n</mat-sidenav-container>\r\n<app-home-page\r\n  [markdownContent]=\"markdown\"\r\n  [title]=\"title\"\r\n  (changedTitle)=\"onChangeTitle($event)\"\r\n  (changedMarkdownContent)=\"onChangeMarkdownContent($event)\"\r\n  [viewMode]=\"currentView\"></app-home-page>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/src/FileSaver.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _about_dialog_about_dialog_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./about-dialog/about-dialog.component */ "./src/app/about-dialog/about-dialog.component.ts");
/* harmony import */ var _settings_dialog_settings_dialog_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./settings-dialog/settings-dialog.component */ "./src/app/settings-dialog/settings-dialog.component.ts");
/* harmony import */ var _save_as_md_dialog_save_as_md_dialog_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./save-as-md-dialog/save-as-md-dialog.component */ "./src/app/save-as-md-dialog/save-as-md-dialog.component.ts");
/* harmony import */ var _sample__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sample */ "./src/app/sample.ts");
/* harmony import */ var _generateHtmlContent__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./generateHtmlContent */ "./src/app/generateHtmlContent.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AppComponent = /** @class */ (function () {
    function AppComponent(dialog) {
        this.dialog = dialog;
        this.title = 'Sample';
        this.markdown = _sample__WEBPACK_IMPORTED_MODULE_6__["default"];
        this.currentView = 'dual';
    }
    AppComponent.prototype.activateDualMode = function () {
        this.currentView = 'dual';
    };
    AppComponent.prototype.activatePreviewMode = function () {
        this.currentView = 'preview';
    };
    AppComponent.prototype.activateOriginalMode = function () {
        this.currentView = 'original';
    };
    AppComponent.prototype.openAboutDialog = function () {
        this.dialog.open(_about_dialog_about_dialog_component__WEBPACK_IMPORTED_MODULE_3__["AboutDialogComponent"]);
    };
    AppComponent.prototype.openSettingsDialog = function () {
        this.dialog.open(_settings_dialog_settings_dialog_component__WEBPACK_IMPORTED_MODULE_4__["SettingsDialogComponent"], {
            hasBackdrop: true,
            width: '50%'
        });
    };
    AppComponent.prototype.onChangeTitle = function ($event) {
        this.title = $event;
    };
    AppComponent.prototype.onChangeMarkdownContent = function ($event) {
        this.markdown = $event;
    };
    AppComponent.prototype.openSaveAsMdDialogComponent = function () {
        this.dialog.open(_save_as_md_dialog_save_as_md_dialog_component__WEBPACK_IMPORTED_MODULE_5__["SaveAsMdDialogComponent"]);
    };
    AppComponent.prototype.saveAsHtml = function () {
        var fileName = this.title.replace(/ /g, '_');
        var contentAsHtml = document.getElementById('markdown-preview').innerHTML;
        var blob = new Blob([Object(_generateHtmlContent__WEBPACK_IMPORTED_MODULE_7__["default"])(fileName, contentAsHtml)], { type: "text/plain;charset=utf-8" });
        Object(file_saver__WEBPACK_IMPORTED_MODULE_2__["saveAs"])(blob, fileName + ".html");
    };
    AppComponent.prototype.saveAsMarkdown = function () {
        var fileName = this.title.replace(/ /g, '_');
        var blob = new Blob([this.markdown], { type: "text/plain;charset=utf-8" });
        Object(file_saver__WEBPACK_IMPORTED_MODULE_2__["saveAs"])(blob, fileName + ".md");
    };
    AppComponent.prototype.readFile = function () {
        var _this = this;
        var file = document.getElementById('fileUpload').files[0];
        var fileReader = new FileReader();
        fileReader.readAsText(file, 'UTF-8');
        fileReader.onload = function (event) {
            _this.markdown = fileReader.result.toString();
            _this.title = _this.removeFileExtension(file.name);
        };
    };
    AppComponent.prototype.removeFileExtension = function (fileName) {
        var fileExtension = fileName.split('.').pop();
        var indexOfFileExtension = fileName.indexOf(fileExtension) - 1;
        return fileName.substr(0, indexOfFileExtension);
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var ngx_markdown__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-markdown */ "./node_modules/ngx-markdown/fesm5/ngx-markdown.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _about_dialog_about_dialog_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./about-dialog/about-dialog.component */ "./src/app/about-dialog/about-dialog.component.ts");
/* harmony import */ var _settings_dialog_settings_dialog_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./settings-dialog/settings-dialog.component */ "./src/app/settings-dialog/settings-dialog.component.ts");
/* harmony import */ var _home_page_home_page_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./home-page/home-page.component */ "./src/app/home-page/home-page.component.ts");
/* harmony import */ var _save_as_md_dialog_save_as_md_dialog_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./save-as-md-dialog/save-as-md-dialog.component */ "./src/app/save-as-md-dialog/save-as-md-dialog.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                _about_dialog_about_dialog_component__WEBPACK_IMPORTED_MODULE_8__["AboutDialogComponent"],
                _settings_dialog_settings_dialog_component__WEBPACK_IMPORTED_MODULE_9__["SettingsDialogComponent"],
                _home_page_home_page_component__WEBPACK_IMPORTED_MODULE_10__["HomePageComponent"],
                _save_as_md_dialog_save_as_md_dialog_component__WEBPACK_IMPORTED_MODULE_11__["SaveAsMdDialogComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                ngx_markdown__WEBPACK_IMPORTED_MODULE_5__["MarkdownModule"].forRoot(),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatListModule"],
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]],
            entryComponents: [
                _about_dialog_about_dialog_component__WEBPACK_IMPORTED_MODULE_8__["AboutDialogComponent"],
                _settings_dialog_settings_dialog_component__WEBPACK_IMPORTED_MODULE_9__["SettingsDialogComponent"],
                _save_as_md_dialog_save_as_md_dialog_component__WEBPACK_IMPORTED_MODULE_11__["SaveAsMdDialogComponent"],
            ]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/generateHtmlContent.ts":
/*!****************************************!*\
  !*** ./src/app/generateHtmlContent.ts ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function generateHtmlContent(title, content) {
    var htmlSkeleton = "\n  <!DOCTYPE html>\n  <html lang=\"en\">\n  <head>\n    <meta charset=\"UTF-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n    <title>" + title + "</title>\n    <link href=\"https://fonts.googleapis.com/css?family=Raleway\" rel=\"stylesheet\">\n    <style>\n    * {\n        font-family: 'Raleway', sans-serif;\n      }\n    </style>\n  </head>\n  <body>\n    " + content + "\n  </body>\n  </html>";
    return htmlSkeleton;
}
/* harmony default export */ __webpack_exports__["default"] = (generateHtmlContent);


/***/ }),

/***/ "./src/app/home-page/home-page.component.css":
/*!***************************************************!*\
  !*** ./src/app/home-page/home-page.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.home {\r\n  height: 100%;\r\n  overflow: hidden;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: space-between;\r\n}\r\n\r\n.input-text-markdown {\r\n  margin: 1em 0.5em;\r\n  margin-top: 4.5em;\r\n  display: flex;\r\n  flex: 1;\r\n  flex-direction: column;\r\n}\r\n\r\n.input-text-markdown-hide {\r\n  display: none;\r\n}\r\n\r\n.input-markdown {\r\n  width: 100%;\r\n  height: 30vh;\r\n  flex: 1;\r\n}\r\n\r\n.markdown-preview {\r\n  margin: 0.5em;\r\n  margin-top: 4.8em;\r\n  padding: 0.5em;\r\n  flex: 1;\r\n  word-break: break-word;\r\n  overflow: auto;\r\n  background: whitesmoke;\r\n  border-radius: 5px;\r\n}\r\n\r\n.markdown-preview-hide {\r\n  display: none;\r\n}\r\n\r\n@media only screen and (min-width: 992px) {\r\n  .home {\r\n    flex-direction: row;\r\n  }\r\n\r\n  .input-markdown {\r\n    height: 42em;\r\n  }\r\n\r\n  .markdown-preview {\r\n    margin-top: 4.8em;\r\n    height: 50em;\r\n  }\r\n} \r\n\r\n"

/***/ }),

/***/ "./src/app/home-page/home-page.component.html":
/*!****************************************************!*\
  !*** ./src/app/home-page/home-page.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"home\">\r\n  <div\r\n    [ngClass]=\"{'input-text-markdown': _viewMode !== 'preview', 'input-text-markdown-hide': _viewMode === 'preview'}\"\r\n    id=\"input-text-markdown-container\"\r\n    (scroll)=\"onElementScroll($event)\"\r\n  >\r\n    <mat-form-field appearance=\"outline\">\r\n      <mat-label>Title (save as file name)</mat-label>\r\n      <input\r\n        type=\"text\"\r\n        matInput\r\n        (keyup)=\"onChangeTitle()\"\r\n        [(ngModel)]=\"_title\" />\r\n    </mat-form-field>\r\n    <mat-form-field appearance=\"outline\">\r\n      <mat-label>Body</mat-label>\r\n      <textarea\r\n        matInput \r\n        class=\"input-markdown\" \r\n        (keyup)=\"onChangeMarkdownContent()\"\r\n        [(ngModel)]=\"_markdownContent\">\r\n      </textarea>\r\n    </mat-form-field>\r\n  </div>\r\n  <div \r\n    [ngClass]=\"{'markdown-preview': _viewMode !== 'original', 'markdown-preview-hide': _viewMode === 'original'}\"\r\n    id=\"markdown-preview-container\">\r\n    <markdown [data]=\"_markdownContent\" place-holder=\"Preview\" id=\"markdown-preview\">Preview</markdown>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/home-page/home-page.component.ts":
/*!**************************************************!*\
  !*** ./src/app/home-page/home-page.component.ts ***!
  \**************************************************/
/*! exports provided: HomePageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageComponent", function() { return HomePageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomePageComponent = /** @class */ (function () {
    function HomePageComponent() {
        this._title = '';
        this._markdownContent = '';
        this._viewMode = '';
        this.changedTitle = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.changedMarkdownContent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    Object.defineProperty(HomePageComponent.prototype, "markdownContent", {
        get: function () { return this._markdownContent; },
        set: function (markdownContent) {
            this._markdownContent = markdownContent;
            this.changedMarkdownContent.emit(this._markdownContent);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HomePageComponent.prototype, "title", {
        get: function () { return this._title; },
        set: function (title) {
            this._title = title;
            this.changedTitle.emit(this._title);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HomePageComponent.prototype, "viewMode", {
        set: function (viewMode) {
            this._viewMode = viewMode;
        },
        enumerable: true,
        configurable: true
    });
    HomePageComponent.prototype.onChangeTitle = function () {
        this.changedTitle.emit(this._title);
    };
    HomePageComponent.prototype.onChangeMarkdownContent = function () {
        this.changedMarkdownContent.emit(this._markdownContent);
    };
    HomePageComponent.prototype.onElementScroll = function (event) {
        console.log(event);
    };
    HomePageComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('markdownContent'),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], HomePageComponent.prototype, "markdownContent", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('title'),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], HomePageComponent.prototype, "title", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('viewMode'),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], HomePageComponent.prototype, "viewMode", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], HomePageComponent.prototype, "changedTitle", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], HomePageComponent.prototype, "changedMarkdownContent", void 0);
    HomePageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home-page',
            template: __webpack_require__(/*! ./home-page.component.html */ "./src/app/home-page/home-page.component.html"),
            styles: [__webpack_require__(/*! ./home-page.component.css */ "./src/app/home-page/home-page.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomePageComponent);
    return HomePageComponent;
}());



/***/ }),

/***/ "./src/app/sample.ts":
/*!***************************!*\
  !*** ./src/app/sample.ts ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var sampleString = "An h1 header\n============\n\nParagraphs are separated by a blank line.\n\n2nd paragraph. *Italic*, **bold**. Itemized lists\nlook like:\n\n  * this one\n  * that one\n  * the other one\n\nNote that --- not considering the asterisk --- the actual text\ncontent starts at 4-columns in.\n\n> Block quotes are\n> written like so.\n>\n> They can span multiple paragraphs,\n> if you like.\n\nUse 3 dashes for an em-dash. Use 2 dashes for ranges (ex., \"it's all\nin chapters 12--14\"). Three dots ... will be converted to an ellipsis.\nUnicode is supported. \u263A\n\n\n\nAn h2 header\n------------\n\nHere's a numbered list:\n\n 1. first item\n 2. second item\n 3. third item\n\nNote again how the actual text starts at 4 columns in (4 characters\nfrom the left side). Here's a code sample:\n\n    # Let me re-iterate ...\n    for i in 1 .. 10 { do-something(i) }\n\nAs you probably guessed, indented 4 spaces. By the way, instead of\nindenting the block, you can use delimited blocks, if you like:\n\n~~~\ndefine foobar() {\n    print \"Welcome to flavor country!\";\n}\n~~~\n\n(which makes copying & pasting easier). You can optionally mark the\ndelimited block for Pandoc to syntax highlight it:\n\n~~~python\nimport time\n# Quick, count to ten!\nfor i in range(10):\n    # (but not *too* quick)\n    time.sleep(0.5)\n    print(i)\n~~~\n\n\n\n### An h3 header ###\n\nNow a nested list:\n\n 1. First, get these ingredients:\n\n      * carrots\n      * celery\n      * lentils\n\n 2. Boil some water.\n\n 3. Dump everything in the pot and follow\n    this algorithm:\n\n        find wooden spoon\n        uncover pot\n        stir\n        cover pot\n        balance wooden spoon precariously on pot handle\n        wait 10 minutes\n        goto first step (or shut off burner when done)\n\n    Do not bump wooden spoon or it will fall.\n\nNotice again how text always lines up on 4-space indents (including\nthat last line which continues item 3 above).\n\nHere's a link to [a website](http://foo.bar), to a [local\ndoc](local-doc.html), and to a [section heading in the current\ndoc](#an-h2-header). Here's a footnote [^1].\n\n[^1]: Some footnote text.\n\nTables can look like this:\n\nName           Size  Material      Color\n------------- -----  ------------  ------------\nAll Business      9  leather       brown\nRoundabout       10  hemp canvas   natural\nCinderella       11  glass         transparent\n\nTable: Shoes sizes, materials, and colors.\n\n(The above is the caption for the table.) Pandoc also supports\nmulti-line tables:\n\n--------  -----------------------\nKeyword   Text\n--------  -----------------------\nred       Sunsets, apples, and\n          other red or reddish\n          things.\n\ngreen     Leaves, grass, frogs\n          and other things it's\n          not easy being.\n--------  -----------------------\n\nA horizontal rule follows.\n\n***\n\nHere's a definition list:\n\napples\n  : Good for making applesauce.\n\noranges\n  : Citrus!\n\ntomatoes\n  : There's no \"e\" in tomatoe.\n\nAgain, text is indented 4 spaces. (Put a blank line between each\nterm and  its definition to spread things out more.)\n\nHere's a \"line block\" (note how whitespace is honored):\n\n| Line one\n|   Line too\n| Line tree\n\nand images can be specified like so:\n\n![example image](example-image.jpg \"An exemplary image\")\n\nInline math equation: $omega = dphi / dt$. Display\nmath should get its own line like so:\n\n$$I = int \rho R^{2} dV$$\n\nAnd note that you can backslash-escape any punctuation characters\nwhich you wish to be displayed literally, ex.: `foo`, *bar*, etc.\n";
/* harmony default export */ __webpack_exports__["default"] = (sampleString);


/***/ }),

/***/ "./src/app/save-as-md-dialog/save-as-md-dialog.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/save-as-md-dialog/save-as-md-dialog.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/save-as-md-dialog/save-as-md-dialog.component.html":
/*!********************************************************************!*\
  !*** ./src/app/save-as-md-dialog/save-as-md-dialog.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2 mat-dialog-title>Save as Mardown</h2>\r\n<mat-dialog-content>\r\n  <mat-form-field appearance=\"outline\">\r\n    <mat-label>File name</mat-label>\r\n    <input\r\n      type=\"text\"\r\n      matInput \r\n      class=\"input-markdown\" \r\n      [(ngModel)]=\"fileName\"/>\r\n  </mat-form-field>\r\n</mat-dialog-content>\r\n<mat-dialog-actions>\r\n  <button class=\"\" mat-button mat-dialog-close>Close</button>\r\n  <button class=\"\" mat-raised-button mat-dialog-close>Save</button>\r\n</mat-dialog-actions>"

/***/ }),

/***/ "./src/app/save-as-md-dialog/save-as-md-dialog.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/save-as-md-dialog/save-as-md-dialog.component.ts ***!
  \******************************************************************/
/*! exports provided: SaveAsMdDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SaveAsMdDialogComponent", function() { return SaveAsMdDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var SaveAsMdDialogComponent = /** @class */ (function () {
    function SaveAsMdDialogComponent(data) {
        this.data = data;
    }
    SaveAsMdDialogComponent.prototype.ngOnInit = function () {
    };
    SaveAsMdDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-save-as-md-dialog',
            template: __webpack_require__(/*! ./save-as-md-dialog.component.html */ "./src/app/save-as-md-dialog/save-as-md-dialog.component.html"),
            styles: [__webpack_require__(/*! ./save-as-md-dialog.component.css */ "./src/app/save-as-md-dialog/save-as-md-dialog.component.css")]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object])
    ], SaveAsMdDialogComponent);
    return SaveAsMdDialogComponent;
}());



/***/ }),

/***/ "./src/app/settings-dialog/settings-dialog.component.css":
/*!***************************************************************!*\
  !*** ./src/app/settings-dialog/settings-dialog.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog-content {\r\n  min-width: 50%;\r\n}\r\n\r\n.close-button {\r\n  display: flex;\r\n  flex: 1;\r\n  justify-content: center;\r\n}"

/***/ }),

/***/ "./src/app/settings-dialog/settings-dialog.component.html":
/*!****************************************************************!*\
  !*** ./src/app/settings-dialog/settings-dialog.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2 mat-dialog-title>Settings</h2>\r\n<mat-dialog-content>\r\n  <h3>General</h3>\r\n  <div style=\"display: inline\">\r\n    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eos sed asperiores vero facilis deserunt eaque expedita! In libero voluptates, nulla iusto laborum asperiores sequi consequuntur ea iure consectetur ex deleniti!</p>\r\n    <mat-slide-toggle checked>Slide me!</mat-slide-toggle>\r\n  </div>\r\n</mat-dialog-content>\r\n<mat-dialog-actions>\r\n  <button class=\"close-button\" mat-stroked-button mat-dialog-close>Close</button>\r\n</mat-dialog-actions>"

/***/ }),

/***/ "./src/app/settings-dialog/settings-dialog.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/settings-dialog/settings-dialog.component.ts ***!
  \**************************************************************/
/*! exports provided: SettingsDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsDialogComponent", function() { return SettingsDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var SettingsDialogComponent = /** @class */ (function () {
    function SettingsDialogComponent(data) {
        this.data = data;
    }
    SettingsDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'settings-dialog',
            template: __webpack_require__(/*! ./settings-dialog.component.html */ "./src/app/settings-dialog/settings-dialog.component.html"),
            styles: [__webpack_require__(/*! ./settings-dialog.component.css */ "./src/app/settings-dialog/settings-dialog.component.css")]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object])
    ], SettingsDialogComponent);
    return SettingsDialogComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\side-project\simple-markdown\core\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map