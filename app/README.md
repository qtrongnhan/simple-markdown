# App build from Electron

## Config
- Run ```ng -build --base-href=./``` in the **core** folder, the ngCLI will generate a built source in ```dist/simple-markdown``` folder.
- Copy the **simple-markdown** folder to the **app/**.

## Start
Start the app by command ```npm start```

## Build
Install ```npm install -g electron-builder-squirrel-windows```
Use ```npm run build``` to build the application.
Check out more info [Electron development page](https://electronjs.org/docs/development)