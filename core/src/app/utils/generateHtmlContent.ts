function generateHtmlContent(title: string, content: string): string {
  const htmlSkeleton: string = `
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>${title}</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <style>
    * {
        font-family: 'Raleway', sans-serif;
      }
    </style>
  </head>
  <body>
    ${content}
  </body>
  </html>`;

  return htmlSkeleton;
}

export default generateHtmlContent;
