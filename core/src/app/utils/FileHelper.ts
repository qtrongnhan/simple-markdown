import { saveAs } from 'file-saver';
import generateHtmlContent from './generateHtmlContent';

export class FileHelper {
  saveAsHtml(title: string, contentAsHtml: string) {
    const fileName = this.removeSpaceInFileName(title);
    const blob = new Blob([generateHtmlContent(fileName, contentAsHtml)], { type: "text/plain;charset=utf-8" });
    saveAs(blob, `${fileName}.html`);
  }

  saveAsMarkdown(title: string, markdown: string) {
    const fileName = title.replace(/ /g, '_');
    const blob = new Blob([markdown], { type: "text/plain;charset=utf-8" });
    saveAs(blob, `${fileName}.md`);
  }

  readFile(): { title: string, markdown: string } {
    let result = { title: '', markdown: '' };
    const file = (<HTMLInputElement>document.getElementById('fileUpload')).files[0];
    const fileReader: FileReader = new FileReader();

    fileReader.onload = (event: Event) => {
      result = {
        title: this.removeFileExtension(file.name),
        markdown: fileReader.result.toString(),
      };
    }

    fileReader.readAsText(file, 'UTF-8');
    console.log(result);
    return result;
  }

  removeFileExtension(fileName: string): string {
    const fileExtension = fileName.split('.').pop();
    const indexOfFileExtension = fileName.indexOf(fileExtension) - 1;
    return fileName.substr(0, indexOfFileExtension);
  }

  removeSpaceInFileName(fileName: string): string {
    return fileName.replace(/ /g, '_');
  }

  constructor() {}

}
