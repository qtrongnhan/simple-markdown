import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'about-dialog',
  templateUrl: './about-dialog.component.html',
  styleUrls: ['./about-dialog.component.css']
})

export class AboutDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }
}
