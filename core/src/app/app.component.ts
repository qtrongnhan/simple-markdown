import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';


import { AboutDialogComponent } from './about-dialog/about-dialog.component';
import { SaveAsMdDialogComponent } from './save-as-md-dialog/save-as-md-dialog.component';

import { FileHelper } from './utils/FileHelper';
import sampleMarkdown from './sample';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title: string = 'Sample';
  markdown: string = sampleMarkdown;
  currentView: string = 'dual';
  fileHelper: FileHelper = new FileHelper();

  activateDualMode() {
    this.currentView = 'dual';
  }

  activatePreviewMode() {
    this.currentView = 'preview';
  }

  activateOriginalMode() {
    this.currentView = 'original';
  }

  openAboutDialog() {
    this.dialog.open(AboutDialogComponent);
  }

  onChangeTitle($event) {
    this.title = $event;
  }

  onChangeMarkdownContent($event) {
    this.markdown = $event;
  }

  openSaveAsMdDialogComponent() {
    this.dialog.open(SaveAsMdDialogComponent);
  }

  saveAsHtml() {
    const contentAsHtml = document.getElementById('markdown-preview').innerHTML;
    this.fileHelper.saveAsHtml(this.title, contentAsHtml);
  }

  saveAsMarkdown() {
    this.fileHelper.saveAsMarkdown(this.title, this.markdown);
  }

  readFile() {
    const data = this.fileHelper.readFile();
    this.title = data.title;
    this.markdown = data.markdown;
  }

  constructor(public dialog: MatDialog) { }

}
