import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  private _title = '';
  private _markdownContent = '';
  private _viewMode = '';

  @Input('markdownContent')
    set markdownContent(markdownContent: string) {
      this._markdownContent = markdownContent;
      this.changedMarkdownContent.emit(this._markdownContent);
    }
    get markdownContent() { return this._markdownContent }
  
  @Input('title')
    set title(title: string) {
      this._title = title;
      this.changedTitle.emit(this._title);
    }
    get title(): string { return this._title }

  @Input('viewMode')
    set viewMode(viewMode: string) {
      this._viewMode = viewMode;
    }

  @Output() changedTitle = new EventEmitter<string>();
  @Output() changedMarkdownContent = new EventEmitter<string>();

  onChangeTitle() {
    this.changedTitle.emit(this._title);
  }

  onChangeMarkdownContent() {
    this.changedMarkdownContent.emit(this._markdownContent);
  }

  constructor() {}

  ngOnInit() {}

}
