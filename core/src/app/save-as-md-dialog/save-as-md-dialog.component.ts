import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-save-as-md-dialog',
  templateUrl: './save-as-md-dialog.component.html',
  styleUrls: ['./save-as-md-dialog.component.css']
})
export class SaveAsMdDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

}
