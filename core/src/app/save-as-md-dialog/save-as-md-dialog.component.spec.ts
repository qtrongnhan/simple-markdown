import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveAsMdDialogComponent } from './save-as-md-dialog.component';

describe('SaveAsMdDialogComponent', () => {
  let component: SaveAsMdDialogComponent;
  let fixture: ComponentFixture<SaveAsMdDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveAsMdDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveAsMdDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
