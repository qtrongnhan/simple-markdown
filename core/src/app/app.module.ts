import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MarkdownModule } from 'ngx-markdown';
import { 
  MatButtonModule,
  MatSidenavModule,
  MatToolbarModule,
  MatMenuModule,
  MatDialogModule,
  MatSlideToggleModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { AboutDialogComponent } from './about-dialog/about-dialog.component';
import { HomePageComponent } from './home-page/home-page.component';
import { SaveAsMdDialogComponent } from './save-as-md-dialog/save-as-md-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutDialogComponent,
    HomePageComponent,
    SaveAsMdDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MarkdownModule.forRoot(),
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatSidenavModule,
    MatToolbarModule,
    MatMenuModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    AboutDialogComponent,
    SaveAsMdDialogComponent,
  ]
})
export class AppModule { }
